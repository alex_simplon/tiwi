class Tamagotchi {
  constructor() {
    this._manger = 100; // Niveau initial de la faim
    this._dormir = 100; // Niveau initial du sommeil
    this._hygiene = 100; // Niveau initial de l'hygiène

    this.startTimer(); // Démarrage du timer
  }

  startTimer() {
    this.displayCountdown(); // Affichage du décompte avant le démarrage du jeu

    setTimeout(() => {
      setInterval(() => {
        this.manger -= 100 / 15; // Diminution de la faim toutes les secondes
        this.dormir -= 100 / 60; // Diminution du sommeil toutes les secondes
        this.hygiene -= 100 / 100; // Diminution de l'hygiène toutes les secondes

        if (this.manger <= 0 || this.dormir <= 0 || this.hygiene <= 0) {
          this.stopGame(); // Arrêt du jeu si un besoin atteint zéro
          this.displayMessage(
            "Tu es un mauvais maître, tu as laissé mourir Tiwi."
          ); // Affichage du message de fin de jeu
          location.reload(); // Recharge la page pour relancer le jeu
        }
      }, 1000); // Timer d'une seconde
    }, 5000); // Délai de 5 secondes avant le démarrage du jeu
  }

  displayCountdown() {
    let count = 5; // Compteur du décompte
    const countdownElement = document.getElementById("countdown"); // Élément HTML où afficher le décompte
    const countdownInterval = setInterval(() => {
      countdownElement.textContent = count; // Affichage du décompte
      count--;

      if (count === -1) {
        clearInterval(countdownInterval); // Arrêt du décompte lorsque le compteur atteint -1
        countdownElement.textContent = ""; // Efface le texte du décompte
      }
    }, 1000); // Timer d'une seconde
  }

  stopGame() {
    clearInterval(); // Arrêt du timer
  }

  displayMessage(message) {
    alert(message); // Affichage d'une fenêtre d'alerte avec le message spécifié
  }

  resetNeeds() {
    this._manger = 100; // Réinitialisation de la faim
    this._dormir = 100; // Réinitialisation du sommeil
    this._hygiene = 100; // Réinitialisation de l'hygiène
  }

  get manger() {
    return this._manger; // Récupération du niveau de faim
  }

  set manger(value) {
    this._manger = value > 100 ? 100 : value; // Assure que la faim ne dépasse pas 100
    this.updateProgressBar("manger-bar", this._manger); // Mise à jour de la barre de progression
  }

  get dormir() {
    return this._dormir; // Récupération du niveau de sommeil
  }

  set dormir(value) {
    this._dormir = value > 100 ? 100 : value; // Assure que le sommeil ne dépasse pas 100
    this.updateProgressBar("dormir-bar", this._dormir); // Mise à jour de la barre de progression
  }

  get hygiene() {
    return this._hygiene; // Récupération du niveau d'hygiène
  }

  set hygiene(value) {
    this._hygiene = value > 100 ? 100 : value; // Assure que l'hygiène ne dépasse pas 100
    this.updateProgressBar("hygiene-bar", this._hygiene); // Mise à jour de la barre de progression
  }

  updateProgressBar(barId, value) {
    const bar = document.getElementById(barId).querySelector(".bar"); // Barre de progression correspondante
    bar.style.width = value + "%"; // Mise à jour de la largeur de la barre

    if (value <= 0) {
      bar.style.backgroundColor = "red"; // Changement de couleur de la barre si le besoin est à zéro
    } else {
      bar.style.backgroundColor = "green"; // Sinon, couleur verte
    }
  }
}

const tamagotchi = new Tamagotchi();

function nourrir() {
  tamagotchi.manger += 100; // Augmentation du niveau de faim
}
